import numpy as np
import sys
import csv
from DT50MarkJoint import *
from LidarMarkJoint import *

# Getting csv data
tEJ=[1532535832.6641,1532535832.6641,1532536262.65189,1532536262.65189]
tEJ=[1532613262.52581,1532613265.52567,1532613416.52137,1532613419.52127] #0


path='/home/dreamer/Documents/bags/HotTestBags/'#'/Users/timeaglezhao/Documents/bags/'#
bagname=['07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-09-49-20',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-14-12',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-30-09',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-002_2018-07-26-10-54-33',\
    '07-25_HotRuns/RadPiper-01_0253_30SteelPipe_2018-08-24-16-31-14',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-12-57',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-36-29',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-54-29',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-17-50',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-29-17',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-39-45',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-17-48',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-32-36',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-48-51',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-15-04-39'
    ]
path=path+bagname[int(sys.argv[1])]
dt50data='/dt50_forward_range.csv'
loc='/.loc_log/optimized_traj.csv'
imgInfo='/img_info.csv'
(tRg,rgL,rgR,lenRg)=readDt50(path+dt50data)
(tLoc,loc,lenLoc)=readLoc(path+loc)
(tImg,img)=readImgInfo(path+imgInfo)

rgL0=rgL
rgR0=rgR
lenRg=len(tRg)


# Demo
dataInput=(tRg,rgL,rgR,tLoc,loc)

mask0=np.array([0.05, 0.08, 0.16, 0.42, 0.16, 0.08, 0.05])
mask1=np.array([2,3,2])
w=4
thrs=0.018
tolerance=2.5
params=(mask0,mask1,w,thrs,tolerance)

#distances = DT50MarkJoints(dataInput)
(distances,(rgL,rgR,locSynced),(rgTP,locTP),(pulseL0,pulseR0,pulseL1,pulseR1),(EJ0,EJ1))\
    = DT50MarkJoint(dataInput,params,1) #Testing mode

# Output result
imgEJ=np.array([],dtype=np.int64)
for i in EJ0:
    imgId=searchImg(tRg[i],tImg,img)-1
    if imgId not in imgEJ:
        imgEJ=np.append(imgEJ,imgId)
print img[imgEJ]

(distances,(rgL,rgR,locSynced),(rgTP,locTP),(pulseL0,pulseR0,pulseL1,pulseR1),(EJ0,EJ1))

# Plotting
rgL=rgL0*1000/25.4#dRgL#
rgR=rgR0*1000/25.4#dRgR#
plt.subplot(211)
#plt.plot(tRg[rgTP],np.ones(4)*rgR[lenRg/2],'ro')
plt.plot(tRg,rgL,'.-',color=(0,0,0.5),label='Left DT50',linewidth=.8)
plt.plot(tRg[np.append(pulseL0,pulseL1)],rgL[np.append(pulseL0,pulseL1)],'o',color=(1,0,1),label='Joints suspected by left DT50')
plt.plot(tRg,rgR,'.-',color=(0,0.5,0),label='Right DT50',linewidth=.8)
plt.plot(tRg[np.append(pulseR0,pulseR1)],rgR[np.append(pulseR0,pulseR1)],'o',color=(1,0.5,0),label='Joints suspected by right DT50')
plt.plot(tEJ,np.ones(4)*rgL[lenRg/2],'ro')
plt.xlim(tRg[rgTP[0]]-50,tRg[rgTP[3]]+50)
plt.ylabel('DT50 readings (in)')
plt.legend()


#plt.legend(bbox_to_anchor=(1,1),loc=1,ncol=2, borderaxespad=0.)#plt.legend()

plt.subplot(212)
plt.plot(tLoc,loc,'.',label='Robot location')
plt.plot(tRg[EJ0],locSynced[EJ0],'ro',label='Detected joints')
plt.plot(tRg[EJ1],locSynced[EJ1],'ro')
plt.xlim(tRg[rgTP[0]]-50,tRg[rgTP[3]]+50)
plt.plot(tLoc[locTP],loc[locTP],'g*')
plt.xlabel('Time (s)')
plt.ylabel('Robot location (in)')
plt.legend()

plt.show()

'''
plt.subplot(312)
plt.plot(tRg,dRgL,'-')
plt.plot(tRg,dRgR,'-')
plt.plot(tEJ,np.ones(4)*dRgL[lenRg/2],'r*')
plt.plot(tEJ,np.ones(4)*dRgR[lenRg/2],'r*')

#.subplot(313)
#plt.plot(tLoc,loc,'.')
plt.plot(tRg,locSynced,'.')
plt.plot(tLoc[locTP],loc[locTP],'ro')
plt.show()
'''
