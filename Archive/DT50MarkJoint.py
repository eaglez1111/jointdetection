import numpy as np
from helpers import *
from funcdef import *

def DT50MarkJoint(dataInput,params=None,testingMode=0):
    (tRg,rgL,rgR,tLoc,loc)=dataInput

    mask0=np.array([0.05, 0.08, 0.16, 0.42, 0.16, 0.08, 0.05])
    mask1=np.array([2,3,2])
    w=4
    thrs=0.018
    dthrs=0.05
    tolerance=2.5
    paramsDefault=(mask0,mask1,w,thrs,tolerance)
    (mask0,mask1,w,thrs,tolerance)=paramsHandler(params,paramsDefault)

    # Process data
    rgL0=rgL
    rgR0=rgR
    locTP=findLocTP(tLoc,loc)
    rgTP=findTP(tRg,tLoc,locTP)
    locSynced=sync(tRg,tLoc,loc)
    lenRg=len(tRg)

    # Smooth DT50 data
    rgL=np.convolve(rgL,mask0,'same')
    rgR=np.convolve(rgR,mask0,'same')
    rgL=np.convolve(rgL,mask1,'same')
    rgR=np.convolve(rgR,mask1,'same')

    # Search for suspious locations from rgL,rgR
    pulseL0=np.array([],dtype=np.int64)
    pulseR0=np.array([],dtype=np.int64)
    pulseL1=np.array([],dtype=np.int64)
    pulseR1=np.array([],dtype=np.int64)
    tMid=(tRg[rgTP[1]]+tRg[rgTP[2]])/2
    for i in range(w*2,lenRg-w*2):
        if tRg[i]<tRg[rgTP[0]] or tRg[i]>tRg[rgTP[3]] or (tRg[i]>tRg[rgTP[1]] and tRg[i]<tRg[rgTP[2]]):
            continue
        if abs(rgL[i]-np.mean(np.append(rgL[i-w*2:i-w],rgL[i+w:i+w*2])))>thrs:
            if tRg[i]<tMid:
                pulseL0=np.append(pulseL0,i)
            else:
                pulseL1=np.append(pulseL1,i)
        if abs(rgR[i]-np.mean(np.append(rgR[i-w*2:i-w],rgR[i+w:i+w*2])))>thrs:
            if tRg[i]<tMid:
                pulseR0=np.append(pulseR0,i)
            else:
                pulseR1=np.append(pulseR1,i)

    # Confirming pulses to be expansion joints by logical combining 4 groups
    EJ0=np.array([],dtype=np.int64)
    EJ1=np.array([],dtype=np.int64)
    for i in np.sort(np.append(pulseL0,pulseR0)):
        j=foundMatchingPulse(locSynced[i],locSynced,np.append(pulseL1,pulseR1),tolerance)
        if np.size(j)!=0:
            if i not in EJ0:
                EJ0=np.append(EJ0,i)
            if j not in EJ1:
                EJ1=np.append(EJ1,j)

    distances=locSynced[EJ0]

    if testingMode:
        return (distances,(rgL,rgR,locSynced),(rgTP,locTP),(pulseL0,pulseR0,pulseL1,pulseR1),(EJ0,EJ1))
    else:
        return distances
