import sys
import numpy as np
from funcdef4test import *
from JointDetect import *
import matplotlib.pyplot as plt


# Main program starts:
path='/home/dreamer/Documents/bags/HotTestBags/'#'/Users/timeaglezhao/Documents/bags/'#

tEJ=np.zeros([20,4],dtype=np.float)
tEJ[0]=[1532613262.52581+27,1532613265.52567+27,1532613416.52137-27,1532613419.52127-27]
bagname=['07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-09-49-20',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-14-12',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-30-09',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-002_2018-07-26-10-54-33',\
    '07-25_HotRuns/RadPiper-01_0253_30SteelPipe_2018-08-24-16-31-14',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-12-57',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-36-29',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-54-29',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-17-50',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-29-17',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-39-45',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-17-48',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-32-36',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-48-51',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-15-04-39'
    ]
num=int(sys.argv[1])
path=path+bagname[num]#'/home/dreamer/Documents/bags/TestingJoshua/RadPiper-01_0253_30LongPipe_2018-10-20-19-40-49'#
loc_csv='/.loc_log/optimized_traj.csv'
lidar_csv='/rplidar_scan.csv'
imgInfo_csv='/img_info.csv'
(tLoc,loc,lenLoc)=readLoc(path+loc_csv)
(tLidar,lidar,lenLidar)=readLidar(path+lidar_csv)
(tImg,img)=readImgInfo(path+imgInfo_csv)
if num>=4 and num<=7:
    pipeDiam=42
else:
    pipeDiam=30

dataInput=(tLidar,lidar,tLoc,loc,tImg,img,path,pipeDiam)
res=JointDetect(dataInput)
(points,peak,trough,locSynced,lidarSector,rawLidar,smoothLidar)=res


# Plotting
plt.plot(tLidar,rawLidar,'b.-',label='rawLidar')
plt.plot(tLidar,smoothLidar,'g.-',label='smoothLidar')
plt.plot(tLidar[np.append(peak[0],peak[1])],smoothLidar[np.append(peak[0],peak[1])],'o',label='peak',color=(1,0.5,0))
plt.plot(tLidar[np.append(trough[0],trough[1])],smoothLidar[np.append(trough[0],trough[1])],'o',label='trough',color=(1,0,1))
if tEJ[num,0]!=0 and 0:
    for i in range(4):
        plt.plot([tEJ[num,i],tEJ[num,i]],[11,19],'k')
plt.ylabel('Lidar Range (in)')
plt.xlabel('Time (sec)')
plt.legend(bbox_to_anchor=(1,1),loc=1,ncol=2, borderaxespad=0.)
plt.show()
