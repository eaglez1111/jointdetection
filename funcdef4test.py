import numpy as np
import csv

# Read DT50 data
def readDt50(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        t=np.zeros(len)
        rgL=np.zeros(len)
        rgR=np.zeros(len)
        i=-1
        for row in spamreader:
            if i>=0:
                t[i]=float(row[2])+float(row[3])/10**9
                rgL[i]=float(row[5])
                rgR[i]=float(row[7])
            i=i+1
    return (t,rgL,rgR,len)

# Read location data
def readLoc(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        loc=np.zeros(len)
        for row in spamreader:
            if i>=0:
                t[i]=float(row[0])
                loc[i]=float(row[1])*1000/25.4
            i=i+1
    return (t,loc,len)

# Read image info
def readImgInfo(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        img=[]
        for row in spamreader:
            if i>=0:
                t[i]=float(row[5])
                img=np.append(img,row[0])
            i=i+1
    return (t,img)

# Read Lidar data
def readLidar(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        lidar=np.zeros([len,360],dtype='float')
        for row in spamreader:
            if i>=0:
                t[i]=float(row[3])+float(row[4])/10**9
                lidar_str=row[-720:-360]
                for j in range(360):
                    lidar[i][j]=float(lidar_str[j])*1000/25.4
            i=i+1
    return (t,lidar,len)
