import cv2
import math
def CVFindJoint(path):
	img = cv2.imread(path)
	resized = cv2.resize(img,None,fx=0.5, fy=0.5, interpolation = cv2.INTER_LINEAR)
	height, width, channels = resized.shape
	gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
	rows, columns= gray.shape
	circles = cv2.HoughCircles(gray,cv2.cv.CV_HOUGH_GRADIENT,dp=1,minDist=100,minRadius=50,maxRadius=int(math.ceil(rows / 2)),param1=100,param2=50)
	if (circles is not None):
		return 1
	else:
		return 0
