import numpy as np

# Handle the params nuple
def paramsHandler(params,paramsDefault):
    lengthDefault=len(paramsDefault)
    paramsReturn=[None]*len(paramsDefault)
    if params==None:
        paramsReturn=paramsDefault
    else:
        length=len(params)
        for i in range(length):
            paramsReturn[i]=params[i]
            if params[i]==None:
                paramsReturn[i]=paramsDefault[i]
        paramsReturn[length:lengthDefault+1]=paramsDefault[length:lengthDefault+1]
    return paramsReturn

# Shift turning findKeyPoints
def sync(t0,t1,d1):
    len0=np.size(t0)
    len1=np.size(t1)
    d0=np.zeros(len0)
    j=0
    breakFlag=0
    if t0[0]<t1[0]:
        t1[0]=t1[0]
    for i in range(len0):
        while t1[j]<=t0[i]:
            j=j+1
            if j>=len1:
                breakFlag=1
                break
        if breakFlag:
            d0[i:len0]=np.ones(len0-i)*d0[i-1]
            break
        else:
            d0[i]=d1[j-1]+(d1[j]-d1[j-1])/(t1[j]-t1[j-1])*(t0[i]-t1[j-1])
    return d0
