import numpy as np
from helpers import *
from funcdef import *
from LidarMarkJoint import *
from CVFindJoint import *

def JointDetect(dataInput,params=None,testingMode=0):
    (tLidar,lidar,tLoc,loc,tImg,img,path,pipeDiam)=dataInput
    tOffset=[27,45]
    paramsDefault=(tOffset)
    (tOffset)=paramsHandler(params,paramsDefault)

    tOffset=tOffset[0]*(pipeDiam==30)+tOffset[1]*(pipeDiam==42)
    locTP=findLocTP(tLoc,loc)
    tTP=tLoc[locTP]

    dataInput=(tLidar,lidar,tLoc,loc,pipeDiam,tTP)
    res=LidarMarkJoint(dataInput)
    (peak,trough,locSynced,lidarSector,rawLidar,smoothLidar)=res

    # Group and locate peak&trough
    cnt=-1
    for j in [peak[0],peak[1],trough[0],trough[1]]:
        cnt=cnt+1
        if len(j)!=1:
            points=np.array([],dtype=np.int64)
            k=np.sort(j)
            dk=np.append((k[1:]-k[:-1]),2)
            f0=0
            for i in range(len(dk)):
                if dk[i]>1:
                    value=(cnt<=1)*np.argmax(rawLidar[k[f0]:k[i]+1])+(cnt>1)*np.argmin(rawLidar[k[f0]:k[i]+1])
                    points=np.append(points,k[f0]+value)
                    f0=i+1
            if cnt<=1:
                peak[cnt]=points
            else:
                trough[cnt-2]=points

    # Confirm with CV
    points=np.array([],dtype=np.int64)
    cnt=-1
    for j in [peak[0],peak[1],trough[0],trough[1]]:
        cnt=cnt+1
        for i in j:
            tJoint=tLidar[i]-(1-2*(cnt in [1,3]))*abs(tOffset)
            print cnt,img[np.argmin(np.abs(tImg-tJoint))],CVFindJoint(path+'/'+img[np.argmin(np.abs(tImg-tJoint))])
            if CVFindJoint(path+'/'+img[np.argmin(np.abs(tImg-tJoint))]) and locSynced[i]>72: # could be optimized
                points=np.append(points,i)
        if cnt<=1:
            peak[cnt]=points
        else:
            trough[cnt-2]=points
        points=np.array([],dtype=np.int64)

    #


    return (points,peak,trough,locSynced,lidarSector,rawLidar,smoothLidar)#(joints)

#
