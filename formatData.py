import numpy as np
import csv
import sys
from funcdef4test import *
from funcdef import *


bagname=['07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-09-49-20',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-14-12',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-30-09',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-002_2018-07-26-10-54-33',\
    '07-25_HotRuns/RadPiper-01_0253_30SteelPipe_2018-08-24-16-31-14',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-12-57',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-36-29',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-54-29',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-17-50',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-29-17',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-39-45',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-17-48',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-32-36',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-48-51',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-15-04-39'] #14
#num=int(sys.argv[1])

for num in range(15):
    path='/home/dreamer/Documents/bags/HotTestBags/'#'/Users/timeaglezhao/Documents/bags/'#
    path=path+bagname[num]
    loc_csv='/.loc_log/optimized_traj.csv'
    lidar_csv='/rplidar_scan.csv'
    (tLoc,loc,lenLoc)=readLoc(path+loc_csv)
    (tLidar,lidar,lenLidar)=readLidar(path+lidar_csv)
    locSynced=sync(tLidar,tLoc,loc)
    lenLidar=len(tLidar)
    if num>=4 and num<=7:
        pipeDiam=42
    else:
        pipeDiam=30

    lidarSector=np.zeros(lenLidar,dtype=np.float);
    for i in range(lenLidar):
        lidarSector[i]=np.mean(lidar[i,0:3]);
    thres=(pipeDiam-18)*1.3
    for i in range(1,int(lenLidar/2),1):
        if lidarSector[i]<thres:
            TP0=i
            break
    for i in range(lenLidar-1,int(lenLidar/2),-1):
        if lidarSector[i]<thres:
            TP3=i
            break

    data=np.zeros([362,TP3-TP0],dtype=np.float);
    data[0]=tLidar[TP0:TP3];
    data[1]=locSynced[TP0:TP3];
    data[2:362]=np.transpose(lidar[TP0:TP3,:]);

    with open('../data/'+str(num)+'.csv', 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for i in range(len(data[0])):
            spamwriter.writerow(data[:,i])

    print num,"done",lenLidar,TP0,TP3,TP3-TP0
    print '-----'






        #
