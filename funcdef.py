import numpy as np
from helpers import *

# Find turning points for timeStamps t based on given TP
def findTP(t,tTP):
    i=0
    j=0
    TP=np.zeros(4,dtype=np.int64)
    while TP[3]==0:
        if t[i]>tTP[j]:
            TP[j]=i
            j=j+1
        i=i+1
    TP[1]=TP[1]-1
    TP[3]=TP[3]-1
    return TP

# update TP for Lidar
def updateLidarTP(TP,lidarSector,thres):
    for i in range(TP[0],TP[1],1):
        if lidarSector[i]<thres:
            TP[0]=i
            break
    for i in range(TP[3],TP[2],-1):
        if lidarSector[i]<thres:
            TP[3]=i
            break
    return TP

# See if pulse appear both on forward and reverse
def foundMatch(p,loc,set,tolerance):
    found=0
    for i in set:
        if abs(loc[i]-loc[p])<tolerance:
            found=1
    return found

# get reasonable range values from lidar
def smallValuesOf(set0,thres):
    set1=np.zeros(np.size(set0))
    cnt=0
    for i in range(np.size(set0)):
        if set0.item(i)<thres:
            set1[cnt]=set0.item(i)
            cnt=cnt+1
    return set1[0:cnt]

# Adaptive threshold
def adaptiveThresCompare(arry,k,m,n,ofst,mode):
    if mode==0:
        if abs(arry[k]-np.mean(arry[k-n:k-m]+arry[k+m:k+n])/2) > ofst:
            return 1
        else:
            return 0
    else:
        if arry[k]-mode*ofst>np.mean(arry[k-n:k-m]+arry[k+m:k+n])/2:
            return (mode==1)
        else:
            return (mode==-1)

# Reture index, whose elements pass adaptiveThres checking
def adaptiveThres(arry,(sta,end),m,n,ofst,mode):
    output=np.zeros(end-sta,dtype=np.int64)
    cnt=0
    for i in range(sta,end):
        if adaptiveThresCompare(arry,i,m,n,ofst,mode):
            output[cnt]=i
            cnt=cnt+1
    return output[0:cnt]

# Find local minimum in a certain pattern based on step&thres
def findTrough(arry,(sta,end),step,thres):
    trough=np.zeros(end-sta,dtype=np.int64)
    cnt=0
    for i in range(sta,end):
        if arry[i]<arry[i+step]-thres and arry[i]<arry[i-step]-thres:
            trough[cnt]=i
            cnt=cnt+1
    return trough[:cnt]

# Confirm with location, return 1 if found on both forward&reverse
def foundOnBothWays(t,joints,locSynced,tolerance):
    confirmed=[np.array([],dtype=np.int64),np.array([],dtype=np.int64)]
    for i in [0,1]:
        for j in joints[i]:
            if foundMatch(j,locSynced,joints[1-i],tolerance) and (j not in confirmed[i]):
                confirmed[i]=np.append(confirmed[i],j)
    return confirmed

# Find location turning points from loc
def findLocTP(t,loc):
    i=0
    j=0
    TP=np.array([0]*4)
    s=0.1#0.4
    while TP[3]==0:
        step=loc[i+1]-loc[i]
        if (j==0 and step>s)|(j==1 and step<s)|(j==2 and step<-s)|(j==3 and step>-s):
            TP[j]=i
            j=j+1
        i=i+1
    TP[1]=TP[1]-1
    TP[3]=TP[3]-1
    return TP














#
