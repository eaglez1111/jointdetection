import numpy as np
from helpers import *
from funcdef import *

def LidarMarkJoint(dataInput,params=None,testingMode=0):
    (tLidar,lidar,tLoc,loc,pipeDiam,tTP)=dataInput

    mask0=np.array([2,2,2,2,2,2,2,2],dtype=np.float)/16
    thres=[0.002,0.002]
    step=[5,5]
    tolerance=2.5
    paramsDefault=(mask0,thres,step,tolerance)
    (mask0,thres,step,tolerance)=paramsHandler(params,paramsDefault)

    thres=thres[0]*(pipeDiam==30)+thres[1]*(pipeDiam==42)
    step=step[0]*(pipeDiam==30)+step[1]*(pipeDiam==42)

    # Process data
    lenLidar=len(tLidar)
    lidarSector=np.zeros([7,lenLidar],dtype=np.float);
    for i in range(lenLidar):
        lidarSector[0:6,i]=[ np.mean(smallValuesOf(np.append(lidar[i,0:15],lidar[i,345:360]),(pipeDiam-18)*1.3)),\
            np.mean( smallValuesOf(lidar[i,15:75]+lidar[i,195:255],pipeDiam*1.3) )/2,\
            np.mean( smallValuesOf(lidar[i,75:105]+lidar[i,255:285],pipeDiam*1.3) )/2,\
            np.mean( smallValuesOf(lidar[i,105:165]+lidar[i,285:345],pipeDiam*1.3) )/2,\
            np.mean( smallValuesOf(lidar[i,165:195],18*1.3) ),\
            np.mean( np.append( smallValuesOf(lidar[i,0:90]+lidar[i,180:270],pipeDiam*1.3),smallValuesOf(lidar[i,90:180]+lidar[i,270:360],pipeDiam*1.3) ) ) /2 ]
    rawLidar=lidarSector[5]
    TP=findTP(tLidar,tTP)
    TP=updateLidarTP(TP,lidarSector[0],(pipeDiam-18)*1.3)
    locSynced=sync(tLidar,tLoc,loc)
    lenLidar=len(tLidar)
    smoothLidar=np.convolve(rawLidar,mask0,'same')

    # Find peak and trough
    peak=[adaptiveThres(rawLidar,(TP[0],TP[1]),5,12,0.03,1),adaptiveThres(rawLidar,(TP[2],TP[3]),5,12,0.03,1)]
    trough=[findTrough(smoothLidar,(TP[0],TP[1]),step,thres),findTrough(smoothLidar,(TP[2],TP[3]),step,thres)]

    # Confirm with locations
    peak=foundOnBothWays(tLidar,peak,locSynced,tolerance)
    trough=foundOnBothWays(tLidar,trough,locSynced,tolerance)


    return (peak,trough,locSynced,lidarSector,rawLidar,smoothLidar)
